import gulp from 'gulp';

/**
 * Load plugins automatically using gulp-load-plugins. This eliminates the need
 * to load plugins manually using individual `require` statements.
 * @see {@link https://www.npmjs.com/package/gulp-load-plugins}
 *
 * 1. Convert hyphenated plugin names to camel case (e.g. browser-sync becomes
 * browserSync).
 * 2. Load any plugin listed in package.json.
 */

import plugins from 'gulp-load-plugins';

const $ = plugins({
  camelize: true, /* 1 */
  pattern: '*' /* 2 */
});


/**
 * Import routes.
 * @see gulp.routes.js
 */

import * as route from './gulp.routes.js';


/**
 * Import tasks.
 * @see gulp.tasks.js
 */

import * as task from './gulp.tasks.js';


/**
 * Browsersync.
 * @see tasks/browser.js
 *
 * 1. Start Browsersync.
 * 2. Reload the browser.
 */

gulp.task('browser:initialise', task.browser.initialise(gulp, $, route.hostname)); /* 1 */
gulp.task('browser:reload', task.browser.reload(gulp, $)); /* 2 */


/**
 * Build tasks.
 * @see tasks/build.js
 */

gulp.task('build:site', task.build(gulp));


/**
 * Delete matched files and/or directories.
 * @see tasks/clean.js
 *
 * 1-4. Delete individual theme asset type directories.
 *
 * Warning: all tasks are performed in the distribution directory.
 */

gulp.task('clean:dist:theme:css', task.clean(gulp, $, route.dist.theme_css)); /* 1 */
gulp.task('clean:dist:theme:fonts', task.clean(gulp, $, route.dist.theme_fonts)); /* 2 */
gulp.task('clean:dist:theme:img', task.clean(gulp, $, route.dist.theme_img)); /* 3 */
gulp.task('clean:dist:theme:js', task.clean(gulp, $, route.dist.theme_js)); /* 4 */


/**
 * Copy matched files and/or directories.
 * @see tasks/copy.js
 *
 * 1. Copy Font Awesome (font files) to the distribution directory.
 * 2. Copy theme font files to the distribution directory.
 */

gulp.task('copy:fonts:font-awesome', task.copy(gulp, $, route.src.font_awesome, route.dist.font_awesome, 'Successfully copied Font Awesome fonts to distribution.')); /* 1 */
gulp.task('copy:fonts', task.copy(gulp, $, route.src.theme_fonts, route.dist.theme_fonts, 'Successfully copied fonts to distribution.')); /* 2 */


/**
 * CSS/SCSS tasks.
 * @see tasks/css.js
 *
 * 1. Compile SCSS to CSS.
 * 2. Minify vanilla CSS.
 */

gulp.task('css:compile', task.css.compile(gulp, $, route.src.theme_scss, route.dist.theme_css, 'Successfully compiled SCSS to CSS.')); /* 1 */
gulp.task('css:minify', task.css.minify(gulp, $, route.src.theme_css, route.dist.theme_css, 'Successfully minified CSS.')); /* 2 */


/**
 * Image tasks.
 * @see tasks/img.js
 *
 * 1. Optimise images.
 * 2. Convert matched images to WebP images. This task is performed in the
 * distribution directory.
 */

gulp.task('img:optimise', task.img.optimise(gulp, $, route.src.theme_img, route.dist.theme_img, 'Successfully optimised images.')); /* 1 */
gulp.task('img:dist:cwebp', task.img.cwebp(gulp, $, `${route.dist.theme_img}/**/*.{jpg,png}`, route.dist.theme_img, 'Successfully generated WebP images.')); /* 2 */


/**
 * JS tasks.
 * @see tasks/js.js
 * @see webpack.config.babel.js
 *
 * 1. Compile JS using Webpack.
 */

gulp.task('js:webpack', task.js.webpack(gulp, $, 'Successfully compiled JS.')); /* 1 */


/**
 * SVG tasks.
 * @see tasks/svg.js
 *
 * 1. Minify SVG files.
 */

gulp.task('svg:minify', task.svg.minify(gulp, $, route.src.theme_svg, route.dist.theme_img, 'Successfully minified SVG files.')); /* 1 */


/**
 * Sequential asset tasks.
 */

gulp.task('do:theme:css', gulp.series('clean:dist:theme:css', 'css:compile', 'css:minify'));
gulp.task('do:theme:fonts', gulp.series('clean:dist:theme:fonts', 'copy:fonts:font-awesome', 'copy:fonts'));
gulp.task('do:theme:img', gulp.series('clean:dist:theme:img', 'img:optimise', 'svg:minify'));
gulp.task('do:theme:js', gulp.series('clean:dist:theme:js', 'js:webpack'));


/**
 * Watch task.
 */

gulp.task('watch', function() {
  gulp.watch(route.src.theme, gulp.series('browser:reload'));
  gulp.watch([route.src.theme_css, route.src.theme_scss], gulp.series('do:theme:css', 'browser:reload'));
  gulp.watch([route.src.theme_img, route.src.theme_svg], gulp.series('do:theme:img', 'browser:reload'));
  gulp.watch(route.src.theme_js, gulp.series('do:theme:js', 'browser:reload'));
});


/**
 * Primary tasks.
 */

gulp.task('deploy', gulp.series(gulp.parallel('do:theme:css', 'do:theme:fonts', 'do:theme:img', 'do:theme:js')));
gulp.task('default', gulp.series('deploy', 'browser:initialise', 'watch'));
