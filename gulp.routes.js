/**
 * The site's hostname. A hostname is required by Browsersync in order to
 * configure a proxy server.
 * @see tasks/browser.js
 */

export const hostname = '<hostname>';


/**
 * High-level directory paths.
 *
 * 1. The current (i.e. project root) directory.
 * 2. Path to the source directory.
 * 3. Path to the distribution directory.
 * 4. Path to the npm packages directory.
 */

export const dir = {};

dir.root = `${__dirname}`; /* 1 */
dir.src = `${dir.root}/src`; /* 2 */
dir.dist = `${dir.root}/dist`; /* 3 */
dir.packages = `${dir.root}/node_modules`; /* 4 */


/**
 * Packages. Paths are specified relative to the project root directory.
 */

export const packages = {};

packages.font_awesome = `${dir.packages}/@fortawesome/fontawesome-free`;


/**
 * Asset directory paths. Paths are specified relative to the source and/or
 * distribution directories.
 * @see dir.src
 * @see dir.dist
 *
 * 1. Path to the asset root directory.
 * 2-6. Paths to individual asset type (e.g. CSS, JS) directories.
 */

export const assets = {};

assets.root = 'assets'; /* 1 */
assets.css = `${assets.root}/css`; /* 2 */
assets.fonts = `${assets.root}/fonts`; /* 3 */
assets.img = `${assets.root}/img`; /* 4 */
assets.js = `${assets.root}/js`; /* 5 */
assets.scss = `${assets.root}/scss`; /* 6 */


/**
 * File extensions. Exclude parent directory paths and use wildcards to denote
 * subdirectories and files.
 */

export const extensions = {};

extensions.config = '**/*.{json,yml}';
extensions.css = '**/*.css';
extensions.fonts = '**/*.{ttf,otf,woff,woff2,eot,svg}';
extensions.gif = '**/*.gif';
extensions.html = '**/*.html';
extensions.ico = '**/*.ico';
extensions.img = '**/*.{jpg,png,gif,ico}';
extensions.jpg = '**/*.jpg';
extensions.js = '**/*.js';
extensions.json = '**/*.json';
extensions.md = '**/*.md';
extensions.php = '**/*.php';
extensions.png = '**/*.png';
extensions.scss = '**/*.scss';
extensions.svg = '**/*.svg';
extensions.templates = '**/*.{html,md,php}';
extensions.yml = '**/*.yml';


/**
 * File paths. Append file extensions to their corresponding asset directory
 * paths (where appropriate) in order to generate file paths relative to the
 * theme directory.
 * @see scope.theme
 */

export const files = {};

files.config = `${extensions.config}`;
files.css = `${assets.css}/${extensions.css}`;
files.fonts = `${assets.fonts}/${extensions.fonts}`;
files.gif = `${assets.img}/${extensions.gif}`;
files.html = `${extensions.html}`;
files.ico = `${assets.img}/${extensions.ico}`;
files.img = `${assets.img}/${extensions.img}`;
files.jpg = `${assets.img}/${extensions.jpg}`;
files.js = `${assets.js}/${extensions.js}`;
files.json = `${extensions.json}`;
files.md = `${extensions.md}`;
files.php = `${extensions.php}`;
files.png = `${assets.img}/${extensions.png}`;
files.scss = `${assets.scss}/${extensions.scss}`;
files.svg = `${assets.img}/${extensions.svg}`;
files.templates = `${extensions.templates}`;
files.yml = `${extensions.yml}`;


/**
 * Webpack entry points. Paths are specified relative to the project root directory.
 * @see webpack.config.babel.js
 */

export const webpack = {};

webpack.entries = {};
webpack.entries.theme = `${dir.src}/${assets.js}/theme.js`;


/**
 * Concatenate partial source directory paths and file extensions into globs for
 * ease of use. Paths are specified relative to the project root directory.
 *
 * 1. Match Font Awesome font files.
 * 2. Match all theme template and theme configuration files.
 * 3-8. Match individual theme asset types.
 */

export const src = {};

src.font_awesome = `${packages.font_awesome}/webfonts/${extensions.fonts}`; /* 1 */
src.theme = [`${dir.src}/${files.templates}`, `${dir.src}/${files.config}`] /* 2 */
src.theme_css = `${dir.src}/${files.css}`; /* 3 */
src.theme_fonts = `${dir.src}/${files.fonts}`; /* 4 */
src.theme_img = `${dir.src}/${files.img}`; /* 5 */
src.theme_js = `${dir.src}/${files.js}`; /* 6 */
src.theme_scss = `${dir.src}/${files.scss}`; /* 7 */
src.theme_svg = `${dir.src}/${files.svg}`; /* 8 */


/**
 * Concatenate partial distribution directory paths into globs for ease of use.
 * Paths are specified relative to the project root directory.
 */

export const dist = {};

dist.font_awesome = `${dir.dist}/${assets.fonts}/font-awesome`;
dist.theme = `${dir.dist}`;
dist.theme_css = `${dir.dist}/${assets.css}`;
dist.theme_fonts = `${dir.dist}/${assets.fonts}`;
dist.theme_img = `${dir.dist}/${assets.img}`;
dist.theme_js = `${dir.dist}/${assets.js}`;
dist.theme_scss = `${dir.dist}/${assets.scss}`;
dist.theme_svg = `${dir.dist}/${assets.svg}`;
