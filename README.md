# gulp-init

## Usage

This repository is intended to provide structure and configuration for the development of a custom theme using [Gulp](https://gulpjs.com/) and [Webpack](https://webpack.js.org/). Files in the source directory are processed and compiled to the distribution directory according to the routes and settings configured in the project's build configuration files.

## Requirements

* Node: [`nodejs.org`](https://nodejs.org)
* npm: [`npmjs.com`](https://npmjs.com)

## Installation

Run `$ npm install` to install dependencies.

## Roadmap

* Document route configuration and presets.
* Document package configuration and presets.
