export function initialise(gulp, plugins, hostname) {
  return (done) => {
    plugins.browserSync({
      notify: false,
      proxy: hostname
    });
    done();
  }
}

export function reload(gulp, plugins) {
  return (done) => {
    plugins.browserSync.reload();
    done();
  }
}
