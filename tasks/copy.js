export default function(gulp, plugins, glob, destination, notification) {
  return () =>
    gulp.src(glob)
    .pipe(gulp.dest(destination))
    .pipe(plugins.notify({
      message: notification,
      onLast: true
    }));
}
