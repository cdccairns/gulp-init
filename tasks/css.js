export function compile(gulp, plugins, glob, destination, notification) {
  return () =>
    gulp.src(glob)
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sassGlob())
    .pipe(plugins.sass()).on('error', plugins.sass.logError)
    .pipe(plugins.postcss([
      plugins.stylelint(),
      plugins.autoprefixer(),
      plugins.cssnano()
    ]))
    .pipe(plugins.rename({
      suffix: '.min'
    }))
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest(destination, {
      overwrite: false
    }))
    .pipe(plugins.notify({
      message: notification,
      onLast: true
    }));
}

export function minify(gulp, plugins, glob, destination, notification) {
  return () =>
    gulp.src(glob)
    .pipe(plugins.postcss([
      plugins.stylelint(),
      plugins.autoprefixer(),
      plugins.cssnano()
    ]))
    .pipe(plugins.rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(destination, {
      overwrite: false
    }))
    .pipe(plugins.notify({
      message: notification,
      onLast: true
    }));
}
