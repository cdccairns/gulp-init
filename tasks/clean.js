export default function(gulp, plugins, glob) {
  return () =>
    plugins.del(glob);
}
