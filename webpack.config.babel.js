import webpack    from 'webpack';
import * as route from './gulp.routes.js';

module.exports = {
  entry: route.webpack.entries,
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'eslint-loader',
      options: {
        failOnError: true,
        failOnWarning: false
      }
    }]
  },
  output: {
    filename: '[name].min.js',
    path: `${route.dir.dist}/${route.assets.js}`
  },
  optimization: {
    minimize: true
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
  ]
};
